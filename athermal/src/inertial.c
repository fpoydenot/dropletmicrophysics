#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include <string.h>
#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))
//#define NO_LUBRICATION 1
#define OSEEN 1
//#define STOKES 1
#define VANDERWAALS 1
/* 
Inertial simulation
*/

long double simulation_time;

long double knudsen_number=0.01L;
long double grav_const=9.00183E-6L;
long double radius_ratio=2.0L;
long double density_ratio=830.0L;
long double lambda=48.0L;
long double hamaker_constant= 1.21e-03L;
long double mathcalS_over_A= 1.49e+04L;
long double rzero=-1L;
long double r1,r2,m1,m2,reduced_mass,m2sm1pm2,m1sm1pm2, cd1, cd2, ggg;

long double epsdt=1E-3L;//1.0E-3L;
long double epshh=1.0E-30L;
long double epstop=1.0E-23L;
long double epsdelta=5.0E-4L;
long double limdelta=1.0E-8L;
long double prerzero=10.0L;
long double pretimezero=0.4L;


long double dt, dt_2;
FILE *out;
char sortie[120];

long double solution[6];

long double u1t,u2t;

void init_geometry(void)
{
	r1=1.0L+radius_ratio;
	r2=1.0L+1.0L/radius_ratio;
	m1=r1*r1*r1;
	m2=r2*r2*r2;
	reduced_mass=m1*m2/(m1+m2);
	m2sm1pm2=m2/(m1+m2);
	m1sm1pm2=m1/(m1+m2);
	
	ggg=grav_const/(knudsen_number*knudsen_number*knudsen_number);

	cd1=3.0L*r1/(8.0L*density_ratio);
	cd2=3.0L*r2/(8.0L*density_ratio);
	u1t=(-1.0L+sqrtl(1.0L+8.0L*cd1*ggg*r1*r1/9.0L))/(2.0L*cd1);
	u2t=(-1.0L+sqrtl(1.0L+8.0L*cd2*ggg*r2*r2/9.0L))/(2.0L*cd2);
}

void derivative(long double *x, long double *deriv_x)
{
	/*
	Computes the derivative of vector x.
	x[0] -> h
	x[1] -> theta
	x[2] -> u_r
	x[3] -> u_theta
	x[4] -> w_r
	x[5] -> w_theta
	*/
	long double r, theta_dot, h, cos_theta, sin_theta;
	long double v1_r, v1_theta, v2_r, v2_theta;
	long double f1_r, f1_theta, f2_r, f2_theta;
	long double fp, fvdw;
	long double upsilon;
	#if defined(OSEEN)
		long double v1_n, v2_n, phi1, phi2, w1_r, w1_theta, w2_r, w2_theta;
	#endif
	if (x[0]>epshh) h=x[0];
	else h=0.0L;
	
	r=r1+r2+h;
	v1_r=x[4]-m2sm1pm2*x[2];
	v1_theta=x[5]-m2sm1pm2*x[3];
	v2_r=x[4]+m1sm1pm2*x[2];
	v2_theta=x[5]+m1sm1pm2*x[3];

	#if defined(VANDERWAALS)
		fvdw = (192*powl(r1,3)*powl(r2,3)*mathcalS_over_A/knudsen_number*powl(hamaker_constant,2)*(r1 + r2 + h))/(powl(2*r1 + h,2)*powl(2*r2 + h,2)*powl(2*(r1 + r2) + h,2)*powl(hamaker_constant + h/knudsen_number,2));
	#else
		fvdw = 0.L;
	#endif
	
	#if defined(OSEEN)
		v1_n=sqrtl(v1_r*v1_r+v1_theta*v1_theta);
		v2_n=sqrtl(v2_r*v2_r+v2_theta*v2_theta);
		phi1=expl(-0.5L*r*(v1_n+v1_r)/density_ratio);
		phi2=expl(-0.5L*r*(v2_n-v2_r)/density_ratio);
	#endif
	theta_dot=x[3]/r;
	cos_theta=cosl(x[1]);
	sin_theta=sinl(x[1]);

	deriv_x[0]=x[2];
	deriv_x[1]=theta_dot;
	
	#if defined(OSEEN)
		w1_r=0.0625L/(r*r*r)*r2*(4.0L*phi2*(3.0L*r*r-2.0L*r1*r1-(r*r1*r1*v2_n)/density_ratio)*v2_r-8.0L*(3.0L*density_ratio*r+r2*r2*v2_r)+phi2*r*(2.0L*density_ratio+r*v2_n)*(12.0L+(r1*r1*v2_theta*v2_theta)/(density_ratio*density_ratio)));
		f1_r=m1*ggg*cos_theta+4.5L*r1*((1.0L+2.0L*cd1*v1_n)*w1_r-(1.0L+cd1*v1_n)*v1_r)+fvdw;
		w1_theta=0.0625L/(r*r*r)*r2*(4.0L*(3.0L*phi2*r*r+r2*r2+phi2*r1*r1)+(2.0L*phi2*r*r1*r1*v2_n)/density_ratio+(phi2*r*r*r1*r1*v2_n*v2_n)/(density_ratio*density_ratio)-phi2*r*r1*r1*(2.0L+(r*v2_n)/density_ratio)*v2_r/density_ratio)*v2_theta;
		f1_theta=-m1*ggg*sin_theta+4.5L*r1*((1.0L+2.0L*cd1*v1_n)*w1_theta-(1.0L+cd1*v1_n)*v1_theta);

		//f2 contains the force exerted by drop 1 on drop 2
		w2_r=0.0625L/(r*r*r)*r1*(4.0L*phi1*(3.0L*r*r-2.0L*r2*r2-(r*r2*r2*v1_n)/density_ratio)*v1_r+8.0L*(3.0L*density_ratio*r-r1*r1*v1_r)-phi1*r*(2.0L*density_ratio+r*v1_n)*(12.0L+(r2*r2*v1_theta*v1_theta)/(density_ratio*density_ratio)));
		f2_r=m2*ggg*cos_theta+4.5L*r2*((1.0L+2.0L*cd2*v2_n)*w2_r-(1.0L+cd2*v2_n)*v2_r)-fvdw;
		w2_theta=0.0625L/(r*r*r)*r1*(4.0L*(3.0L*phi1*r*r+r1*r1+phi1*r2*r2)+(2.0L*phi1*r*r2*r2*v1_n)/density_ratio+(phi1*r*r*r2*r2*v1_n*v1_n)/(density_ratio*density_ratio)+phi1*r*r2*r2*(2.0L+(r*v1_n)/density_ratio)*v1_r/density_ratio)*v1_theta;
		f2_theta=-m2*ggg*sin_theta+4.5L*r2*((1.0L+2.0L*cd2*v2_n)*w2_theta-(1.0L+cd2*v2_n)*v2_theta);
	#elif defined(STOKES)
		// Long range Stokes force
		f1_r=4.5L*r1*(u1t*cos_theta-v1_r+0.5L*r2/r*(3.0L-(r1*r1+r2*r2)/(r*r))*v2_r);
		f1_theta=4.5L*r1*(-u1t*sin_theta-v1_theta+0.25L*r2/r*(3.0L+(r1*r1+r2*r2)/(r*r))*v2_theta);
		f2_r=4.5L*r2*(u2t*cos_theta-v2_r+0.5L*r1/r*(3.0L-(r1*r1+r2*r2)/(r*r))*v1_r);
		f2_theta=4.5L*r2*(-u2t*sin_theta-v2_theta+0.25L*r1/r*(3.0L+(r1*r1+r2*r2)/(r*r))*v1_theta);
	#else
		f1_r=0.L;
		f1_theta=0.L;
		f2_r=0.L;
		f2_theta=0.L;
	#endif
	// Lubrication force
	#ifdef NO_LUBRICATION
		fp=0.0L;// No lubrication force
	#else
		// FULL MODEL
		
		upsilon = 1.L+2.L/M_PI*log1pl(knudsen_number/h/3.L);
		fp = 1/(3.L*knudsen_number*upsilon)*((1.L+(h+8.L/7.L*sqrtl(h)/lambda)/(6.L*knudsen_number))*log1pl(6.L*knudsen_number/(h+(8.L/7.L)*sqrtl(h)/lambda))-1.L);
		
		// ONLY SLIP
		//upsilon = 1.L;
		//fp = 1/(3.L*knudsen_number*upsilon)*((1.L+(h+8.L/7.L*sqrtl(h)/lambda)/(6.L*knudsen_number))*log1pl(6.L*knudsen_number/(h+(8.L/7.L)*sqrtl(h)/lambda))-1.L);

		// VALUE FOR KN = 0
		//fp = (7.L*lambda)/(sqrtl(h)*(8.L + 7.L*sqrtl(h)* lambda));
	#endif

	deriv_x[2]=f2_r/m2-f1_r/m1-4.5*fp*x[2]/reduced_mass+x[3]*theta_dot;
	deriv_x[3]=f2_theta/m2-f1_theta/m1-x[2]*theta_dot;

	deriv_x[4]=(f2_r+f1_r)/(m1+m2)+x[5]*theta_dot;
	deriv_x[5]=(f2_theta+f1_theta)/(m1+m2)-x[4]*theta_dot;
}



void runge_kutta(long display_flag)
{
	long i,n;
	long double dxs[6],dxm[6],dxt[6],x[6];

	simulation_time=0.0L;
	if (display_flag>0)
	{
		fprintf(out, "t\tr\th\ttheta\tu_r\tu_theta\tw_r\tw_theta\n");
	}

	n=0;
	do
	{
		// Adaptative timestep; constant timestepping of 1e-8 is convergent but slower.
		dt=epsdt*MIN(1.0L,solution[0]+1.0L*knudsen_number)/(u1t-u2t);
		dt_2=0.5L*dt;
		n++;
		for (i=0;i<=5;i++) x[i]=solution[i];
		derivative(x, dxs); /* k1=dt*dxs */
		for (i=0;i<=5;i++) x[i]=solution[i]+dt_2*dxs[i];
		derivative(x, dxt); /* k2=dt*dxt */
		for (i=0;i<=5;i++) x[i]=solution[i]+dt_2*dxt[i];
		derivative(x, dxm); /* k3=dt*dxm */
		for (i=0;i<=5;i++) {x[i]=solution[i]+dt*dxm[i]; dxm[i] += dxt[i]; } //k2+k3=dt*dxm
		derivative(x, dxt);
		for (i=0;i<=5;i++) solution[i]+=dt*(dxs[i]+dxt[i]+2.0*dxm[i])/6.0;
		simulation_time+=dt;
		if ((display_flag>0)&&(n%100==0))
		{
			fprintf(out, "%.14Le\t%.14Le\t%.14Le\t%.14Le\t%.14Le\t%.14Le\t%.14Le\t%.14Le\n", simulation_time, solution[0]+r1+r2, solution[0], solution[1], solution[2], solution[3], solution[4], solution[5]);
		}
	}
	while ((solution[0] > epstop)&&(solution[2] < -epstop));
}

void init_solution(long double delta)
{
	solution[0]=rzero-(r1+r2);
	solution[1]=asinl(delta/rzero);
	solution[2]=(u2t-u1t)*cosl(solution[1]);
	solution[3]=-(u2t-u1t)*sinl(solution[1]);
	solution[4]=(m1sm1pm2*u1t+m2sm1pm2*u2t)*cosl(solution[1]);
	solution[5]=-(m1sm1pm2*u1t+m2sm1pm2*u2t)*sinl(solution[1]);
}

long double root_finder(long double delta)
{
	long double vg,vd;

	init_solution(delta);
	runge_kutta(0);
	if (solution[0]>1.0E-23L)
	{
		do
		{
			vd=delta;
			delta*=0.9L;
			init_solution(delta);
			runge_kutta(0);
		}
		while ((solution[0]>1.0E-23L) && (delta > limdelta));
		vg=delta;
	}
	else
	{
		do
		{
			vg=delta;
			delta*=1.1L;
			init_solution(delta);
			runge_kutta(0);
		}
		while (solution[0]<=1.0E-23L);
		vd=delta;
	}

	if (delta > limdelta)
	{
		do
		{
			delta=0.5L*(vg+vd);
			init_solution(delta);
			runge_kutta(0);
			if (solution[0]>1.0E-23L) vd=delta;
			else vg=delta;
		}
		while ((vd-vg)>epsdelta*(vd+vg));
		delta=0.5L*(vg+vd);
	}
	return delta;
}
/****************************************************************/
/**************************** Computation ****************************/
/****************************************************************/

int main(int argc, char *argv[])
{
	long double efficiency;
	long double delta = -1L;
	long double delta_prev = 1L;
	long double max_rzero = 1E8L;
	int opt;
	char *path_input = NULL;
	char *path = NULL;

	if(argc == 1)
	{
		fprintf(stderr, "Options :\n-r\t radius ratio > 1\n-l\t Knudsen number (mean free path/a)\n-d\t initial impact parameter\n-o\t output filename to save trajectory.\n");
		exit(0);
	}
	// Constants initialisation
	while ((opt = getopt(argc, argv, "l:r:o:d:")) != -1)
		switch (opt)
		{
			case 'l':
				knudsen_number = strtold(optarg, NULL);
				break;
			case 'r':
				radius_ratio = strtold(optarg, NULL);
				break;
			case 'o':
				path_input = optarg;
				fprintf(stderr, "path = %s\n", path_input);
				break;
			case 'd':
				delta = strtold(optarg, NULL);
				delta_prev = -1L;
				break;
			case '?':
				if (optopt == 'l' || optopt == 'r')
				{
					fprintf(stderr, "No input value.\n");
					fprintf(stderr, "Options :\n-r\t radius ratio > 1\n-l\t Knudsen number (mean free path/a)\n-d\t initial impact parameter\n-o\t output filename to save trajectory.\n");
				}
				else if (isprint (optopt))
					fprintf(stderr, "Unknown option `-%c'.\n", optopt);
				else
					fprintf(stderr, "Unknown option character `\\x%x'.\n", optopt);
				abort();
			default:
				abort();
		}
	init_geometry();
	if(rzero < 0.L)
		rzero = prerzero*(r1+r2)+(u1t-u2t)*pretimezero;

	fprintf(stderr, "knudsen_number = %.14Le\n", knudsen_number);
	fprintf(stderr, "radius_ratio = %.14Le\n", radius_ratio);
	fprintf(stderr, "rzero = %Le\n", rzero);
	fprintf(stderr, "max_rzero = %Le\n", max_rzero);
	fprintf(stderr, "grav_const = %Le\n", grav_const);
	fprintf(stderr, "grav_const/Kn^3 = %Le\n", ggg);
	fprintf(stderr, "lambda = %Le\n", lambda);
	fprintf(stderr, "density_ratio = %Le\n", density_ratio);
	fprintf(stderr, "ut1 = %Le\n", u1t);
	fprintf(stderr, "ut2 = %Le\n", u2t);
	fprintf(stderr, "Re 1 = %Le\n", r1*u1t/density_ratio);
	fprintf(stderr, "Re 2 = %Le\n", r2*u2t/density_ratio);
	fprintf(stderr, "ut1(Re = 0) = %Le\n", 2.0L*ggg*r1*r1/9.0L);
	fprintf(stderr, "ut2(Re = 0) = %Le\n", 2.0L*ggg*r2*r2/9.0L);

	#if defined(NO_LUBRICATION)
		fprintf(stderr, "No lubrication force.\n");
	#else
		fprintf(stderr, "Lubrication force enabled.\n");
	#endif
	#if defined(OSEEN)
		fprintf(stderr, "Oseen long range.\n");
	#elif defined(STOKES)
		fprintf(stderr, "Stokes long range.\n");
	#else
		fprintf(stderr, "No long range.\n")
	#endif
	#if defined(VANDERWAALS)
		fprintf(stderr, "Van der Waals enabled. hamaker_constant = %Le, mathcalS_over_A = %Le\n", hamaker_constant, mathcalS_over_A);
	#endif

	fprintf(stderr, "Inertial computation start.\n");
	
	// Compute using determinist formula
	fprintf(stderr, "Looking for critical trajectory...\n");
	delta=0.99L*(r1+r2)/1.1L;
	init_solution(delta);
	delta = root_finder(delta);
	fprintf(stderr, "delta = %Le\tdelta_prev = %Le\tE = %Le\tr0 = %Le\n", delta, 0.99L*(r1+r2)/1.1L, (delta/(r1+r2))*(delta/(r1+r2)), rzero);


	// Compute efficiency until convergence.
	/*if (delta_prev >= 0)
	{
		fprintf(stderr, "Looking for critical trajectory...\n");
		delta=0.99L*(r1+r2)/1.1L;
		do
		{
			delta_prev = delta;
			init_solution(delta);
			delta = root_finder(delta);
			fprintf(stderr, "delta = %Le\tdelta_prev = %Le\tE = %Le\tr0 = %Le\n", delta, delta_prev, (delta/(r1+r2))*(delta/(r1+r2)), rzero);
			rzero=rzero*1.5;
		}
		while((fabsl(delta-delta_prev)/delta > 1E-2L) && (rzero < max_rzero) && (delta > 1E-24L));
		rzero = rzero/1.5;
	}
	else
	{
		fprintf(stderr, "delta = %Le. Computing single trajectory...\n", delta);
	}*/

	efficiency=(delta/(r1+r2))*(delta/(r1+r2));
	
	// To compute and save the hit trajectory: 
	if (path_input != NULL)
	{
		path = strdup(path_input);
		strcat(path, ".csv");
		if ((out = fopen(path, "w")) == NULL)
		{
			fprintf(stderr, "Probleme d'ouverture de %s\n", path);
			exit(1);
		}
		fprintf(stderr, "Saving hit trajectory...\n");
		init_solution(delta);
		runge_kutta(1);
		fflush(out);
		fclose(out);
		free(path);
	}
	printf("%.14Le\t%.14Le\t%.14Le\t%.14Le\t%.14Le\n", rzero, knudsen_number, radius_ratio, delta, efficiency);

	fprintf(stderr, "All done.\n");
	return 0;
}

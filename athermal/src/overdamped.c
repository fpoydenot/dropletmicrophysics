#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include <string.h>
#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))
//#define NO_LUBRICATION 1
#define VANDERWAALS 1

/* 
Overdamped simulation
*/

long double simulation_time;

long double knudsen_number=0.01L;
long double radius_ratio=2.0L;
long double lambda=48.0L;
long double hamaker_constant=1.19e-3L;
long double mathcalS_over_A=1.4E4L;
long double grav_const=9.00183E-6L;

long double rzero=-1L;
long double r1,r2,m1,m2,reduced_mass,m2sm1pm2,m1sm1pm2;
long double matrix[2][2];
long double vector[2];
long double dt, dt_2;
FILE *out;
char sortie[120];

long double solution[3];

long double u1t,u2t;

void init_geometry(void)
{
	r1=1.0L+radius_ratio;
	r2=1.0L+1.0L/radius_ratio;
	m1=r1*r1*r1;
	m2=r2*r2*r2;
	reduced_mass=m1*m2/(m1+m2);
	m2sm1pm2=m2/(m1+m2);
	m1sm1pm2=m1/(m1+m2);
	u1t=radius_ratio*radius_ratio;
	u2t=1.L;
}

void invert_matrix(void)
{
	long double inverse_determinant;
	long double v0, v1;
	inverse_determinant = 1/(matrix[0][0]*matrix[1][1]-matrix[0][1]*matrix[1][0]);
	v0 = inverse_determinant*(vector[0]*matrix[1][1]-matrix[0][1]*vector[1]);
	v1 = inverse_determinant*(matrix[0][0]*vector[1]-vector[0]*matrix[1][0]);
	vector[0] = v0;
	vector[1] = v1;
}


void derivative_overdamped(long double *x, long double *deriv_x)
{
	/*
	Computes the derivative of vector x.
	x[0] -> h
	x[1] -> theta
	x[2] -> u_r
	*/
	long double r, h, cos_theta, sin_theta;
	long double v1_r, v1_theta, v2_r, v2_theta;
	long double fp, fvdw;
	long double upsilon;

	if (x[0]>0) h=x[0];
	else h=0.0L;

	r=r1+r2+h;

	#if defined(VANDERWAALS)
		fvdw = (mathcalS_over_A*powl(knudsen_number, 2)*powl(hamaker_constant,2)/grav_const/powl(1+1/radius_ratio, 2))*(192*powl(r1,3)*powl(r2,3)*(r1 + r2 + h))/(powl(2*r1 + h,2)*powl(2*r2 + h,2)*powl(2*(r1 + r2) + h,2)*powl(hamaker_constant + h/knudsen_number,2));
	#else
		fvdw = 0.L;
	#endif

	// Lubrication force
	#ifdef NO_LUBRICATION
		fp=0.0L;// No lubrication force
	#else
		upsilon = 1.L+2.L/M_PI*log1pl(knudsen_number/h/3.L);
		fp = 1/(3.L*knudsen_number*upsilon)*((1.L+(h+8.L/7.L*sqrtl(h)/lambda)/(6.L*knudsen_number))*log1pl(6.L*knudsen_number/(h+(8.L/7.L)*sqrtl(h)/lambda))-1.L);
	#endif

	cos_theta=cosl(x[1]);
	sin_theta=sinl(x[1]);

	matrix[0][0]=r1+fp;
	matrix[0][1]=-0.5L*r1*r2/r*(3.0L-(r1*r1+r2*r2)/(r*r))-fp;
	matrix[1][1]=r2+fp;
	matrix[1][0]=matrix[0][1];
	vector[0]=r1*u1t*cos_theta+fvdw;
	vector[1]=r2*u2t*cos_theta-fvdw;
	invert_matrix();
	v1_r=vector[0];
	v2_r=vector[1];

	matrix[0][0]=1.L;
	matrix[0][1]=-0.25L*r2/r*(3.0L+(r1*r1+r2*r2)/(r*r));
	matrix[1][1]=1.L;
	matrix[1][0]=-0.25L*r1/r*(3.0L+(r1*r1+r2*r2)/(r*r));
	vector[0]=-u1t*sin_theta;
	vector[1]=-u2t*sin_theta;
	invert_matrix();
	v1_theta=vector[0];
	v2_theta=vector[1];
	
	deriv_x[0]=(v2_r-v1_r);
	deriv_x[1]=(v2_theta-v1_theta)/r;
	deriv_x[2]=0;
	solution[2] = (v2_r-v1_r);
}

void runge_kutta_overdamped(long display_flag)
{
	long i,n;
	long double dxs[3], dxm[3], dxt[3], x[3];

	simulation_time = 0.0L;
	if (display_flag>0)
	{
		fprintf(out, "t\tr\th\ttheta\tu_r\n");
	}

	n=0;
	do
	{
		//dt = MIN(1.0E-7L+1E-1L*fabsl(solution[0]/solution[2]), 1E-3L);
		dt = 1e-3L;
		dt_2=0.5L*dt;
		n++;
		for (i=0;i<=2;i++) x[i]=solution[i];
		derivative_overdamped(x, dxs); /* k1=dt*dxs */
		for (i=0;i<=2;i++) x[i]=solution[i]+dt_2*dxs[i];
		derivative_overdamped(x, dxt); /* k2=dt*dxt */
		for (i=0;i<=2;i++) x[i]=solution[i]+dt_2*dxt[i];
		derivative_overdamped(x, dxm); /* k3=dt*dxm */
		for (i=0;i<=2;i++) {x[i]=solution[i]+dt*dxm[i]; dxm[i] += dxt[i]; } //k2+k3=dt*dxm
		derivative_overdamped(x, dxt);
		for (i=0;i<=2;i++) solution[i]+=dt*(dxs[i]+dxt[i]+2.0*dxm[i])/6.0;
		simulation_time+=dt;
		if ((display_flag>0)&&(n%100==0))
		{
			fprintf(out, "%.14Le\t%.14Le\t%.14Le\t%.14Le\t%.14Le\n", simulation_time, solution[0]+r1+r2, solution[0], solution[1], solution[2]);
		}
	}
	while ((solution[0]>1.0E-23L)&&(solution[2]<-1.0E-23L));
}

void init_solution(long double delta)
{
	solution[0] = rzero-(r1+r2);
	if (delta > rzero)
	{
		fprintf(stderr, "Wrong delta (too large): %Le while r0 = %Le\n", delta, rzero);
		exit(EXIT_FAILURE);
	}
	else solution[1] = asinl(delta/rzero);
	solution[2] = -(u1t-u2t)*cosl(solution[1]);
}

long double root_finder(long double delta)
{
	long double vg,vd;

	init_solution(delta);
	runge_kutta_overdamped(0);
	if (solution[0]>1.0E-23L)
	{
		do
		{
			vd=delta;
			delta*=0.9L;
			init_solution(delta);
			runge_kutta_overdamped(0);
		}
		while (solution[0]>1.0E-23L);
		vg=delta;
	}
	else
	{
		do
		{
			vg=delta;
			delta*=1.1L;
			init_solution(delta);
			runge_kutta_overdamped(0);
		}
		while (solution[0]<=1.0E-23L);
		vd=delta;
	}

	do
	{
		delta=0.5L*(vg+vd);
		init_solution(delta);
		runge_kutta_overdamped(0);
		if (solution[0]>1.0E-23L) vd=delta;
		else vg=delta;
	}
	while ((vd-vg)>1.0E-14L);
	delta=0.5L*(vg+vd);
	return delta;
}
/****************************************************************/
/**************************** calcul ****************************/
/****************************************************************/

int main(int argc, char *argv[])
{
	long double efficiency;
	long double delta = -1L;
	long double delta_prev = 1L;
	int opt;
	char *path_input = NULL;
	char *path = NULL;

	if(argc == 1)
	{
		fprintf(stderr, "Options :\n-r\t radius ratio > 1\n-l\t Knudsen number (mean free path/a)\n-d\t initial impact parameter\n-o\t output filename to save trajectory.\n");
		exit(0);
	}
	// Constants initialisation
	while ((opt = getopt(argc, argv, "l:r:o:0:v:d:")) != -1)
		switch (opt)
		{
			case 'l':
				knudsen_number = strtold(optarg, NULL);
				break;
			case 'r':
				radius_ratio = strtold(optarg, NULL);
				break;
			case 'o':
				path_input = optarg;
				fprintf(stderr, "path = %s\n", path_input);
				break;
			case 'd':
				delta = strtold(optarg, NULL);
				delta_prev = -1L;
				break;
			case '?':
				if (optopt == 'l' || optopt == 'r')
				{
					fprintf(stderr, "No input value.\n");
					fprintf(stderr, "Options :\n-r\t radius ratio > 1\n-l\t Knudsen number (mean free path/a)\n-d\t initial impact parameter\n-o\t output filename to save trajectory.\n");
				}
				else if (isprint (optopt))
					fprintf(stderr, "Unknown option `-%c'.\n", optopt);
				else
					fprintf(stderr, "Unknown option character `\\x%x'.\n", optopt);
				abort();
			default:
				abort();
		}

	init_geometry();
	if(rzero < 0.L)
		rzero = 1E1L*(r1+r2);

	fprintf(stderr, "knudsen_number = %.14Le\n", knudsen_number);
	fprintf(stderr, "radius_ratio = %.14Le\n", radius_ratio);
	fprintf(stderr, "rzero = %Le\n", rzero);
	fprintf(stderr, "lambda = %Le\n", lambda);
	#ifdef NO_LUBRICATION
		fprintf(stderr, "No lubrication force.\n");
	#else
		fprintf(stderr, "Lubrication force enabled.\n");
	#endif
	#if defined(VANDERWAALS)
		fprintf(stderr, "Van der Waals enabled. hamaker_constant = %Le, mathcalS_over_A = %Le\n", hamaker_constant, mathcalS_over_A);
	#endif
	fprintf(stderr, "Computation start.\n");

	if (delta_prev >= 0)
	{
		fprintf(stderr, "Looking for critical trajectory...\n");
		delta=0.2L*(r1+r2)/1.1L;
		do
		{
			delta_prev = delta;
			init_solution(delta);
			delta = root_finder(delta);
			fprintf(stderr, "delta = %Le\tdelta_prev = %Le\tE = %Le\tr0 = %Le\n", delta, delta_prev, (delta/(r1+r2))*(delta/(r1+r2)), rzero);
			rzero=rzero*1.5L;
		}
		while((fabsl(delta-delta_prev)/delta > 1E-2L) && (rzero < 1E8L) && (delta > 1E-24L));
		rzero = rzero/1.5L;
	}
	else
	{
		fprintf(stderr, "delta = %Le. Computing single trajectory...\n", delta);
	}
	efficiency=(delta/(r1+r2))*(delta/(r1+r2));
	if (path_input != NULL)
	{
		path = strdup(path_input);
		strcat(path, ".csv");
		if ((out = fopen(path, "w")) == NULL)
		{
			fprintf(stderr, "Probleme d'ouverture de %s\n", path);
			exit(1);
		}
		fprintf(stderr, "Saving hit trajectory to %s...\n", path);
		init_solution(delta);
		runge_kutta_overdamped(1);
		fflush(out);
		fclose(out);
		free(path);
	}
	fprintf(stdout, "%.14Le\t%.14Le\t%.14Le\t%.14Le\t%.14Le\n", rzero, knudsen_number, radius_ratio, delta, efficiency);
	fprintf(stderr, "All done.\n");
	return 0;

}

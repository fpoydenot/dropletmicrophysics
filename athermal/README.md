# Gravity kernel efficiency simulation

## Usage

```bash
mkdir build
make clean && make # to build with lubrication force
make clean && make CPPFLAGS=-DNO_LUBRICATION # to build without lubrication force

./build/inertial -r [radius ratio larger than 1] -l [mean free path over reduced mean radius]
./build/overdamped -r [radius ratio larger than 1] -l [mean free path over reduced mean radius]
```
Debug info outputs to `stderr`; the code outputs on `stdout` a tab-separated line:
```
[initial distance]	[mean free path over reduced mean radius]	[radius ratio]	[impact parameter delta]	[efficiency]
```
The simulation output can be redirected to the end of a file, while still displaying the debug info to the terminal:
```bash
./build/inertial -r [radius ratio larger than 1] -l [mean free path over reduced mean radius] 1>> output.csv
```

# Gravity kernel efficiency simulation

## Usage

```bash
mkdir build
make clean && make # to build with lubrication force
build/main -g [gamma] -a [mathcal A] -n [gridpoints] -o [output file] -l [log file]
```

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include <string.h>
#include <time.h>
#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))


/*********** declaration functions ***********/
long double ran0(void);
long double ran2(long *idum);
double gaussian(long *idum);

/* 
 Inertial simulation with brownian motion
 */

long it,isem = 987654321;
long islu; //seed for the random number generator

long double simulation_time,total_time;
long double epsdt=0.1L;
long double epshh=1.0E-30L;
long double epstop=1.0E-23L;
long double epsdelta=1.0E-6L;
long nb_trajectories=50;
long double limdelta=1.0E-8L;
long double prerzero=20.0L;
long double pretimezero=0.4L;
long double lambda= 3.L;//2.0L;
long double beta = 3.0L;
long double probastop;
long double aaa=100.0L;//a/\bar l
long double ggg;//
long double gsacube=9.00183E-6L;
long double gamra=2.0L; //radius ratio
long double ttt=1.21E-3L;
long double ddd=830.0L;
long double nnn=48.0L;
long double mathcalK = 0.04222572524923858L;
long double hamaker_constant=2.30e-03L;
long double sssaa=3.97e+03L;
long double unsgam,gam,prevdw;
long double RR[2],mm[2],mu_m,msm1pm2[2],unsm[2],ktsm[2];
long double mg[2][3];
long double rey1;
long double rey2;
long double vg,vd;
long double cd[3];
long double rzero_det = 8.0L;
long double rzero=8.0L;
long double rout = 10.L;
long double sigxx[2];
long double sigvv[2];
long double sigxv[2];
long double sigcavv[2];
long double corxv[2];
long double unstau[2];
long double tau[2];
long double ut[2];
long double ddPe[2];
long double ugal;
long double delta,deltax,deltay=0.0L;
long ChocBool;
long double zeta[13][13];
long double xvnoise[3][13];
long double solution[13];
long double vector[2];
long double halfwidth,width;
long double seuilbas,seuilhaut;
long double efficiency;
long double Peclet;
long double dt, dt_2;
FILE *out;
FILE *out_log;
char sortie[120];


void init_geometry(void)
{
	long ii;
	ggg=gsacube*aaa*aaa*aaa;
	gam=sssaa*aaa;
	unsgam=1.0L/gam;
	RR[0]=1.0L+gamra;
	RR[1]=1.0L+1.0L/gamra;
	for (ii=0;ii<2;ii++)
	{
		mm[ii]=RR[ii]*RR[ii]*RR[ii];
		cd[ii]=3.0L*RR[ii]/(8.0L*ddd);
		ut[ii]=(-1.0L+sqrtl(1.0L+8.0L*cd[ii]*ggg*RR[ii]*RR[ii]/9.0L))/(2.0L*cd[ii]);
		unstau[ii]=4.5*(1.0L+cd[ii]*ut[ii])/(RR[ii]*RR[ii]);
		tau[ii]=1.0L/unstau[ii];
		mg[ii][0]=0.0L;
		mg[ii][1]=0.0L;
		mg[ii][2]=mm[ii]*ggg;
		ddPe[ii]=mathcalK/aaa/mm[ii]*tau[ii];
		unsm[ii]=4.5L/mm[ii];
		ktsm[ii]=mathcalK/aaa/mm[ii];
	}

	mu_m=mm[0]*mm[1]/(mm[0]+mm[1]);
	msm1pm2[0]=mm[0]/(mm[0]+mm[1]);
	msm1pm2[1]=mm[1]/(mm[0]+mm[1]);
	ugal=msm1pm2[0]*ut[0]+msm1pm2[1]*ut[1];//translation velocity
	halfwidth= lambda*(RR[0]+RR[1]);
	width=2.0L*halfwidth;

	rzero_det = prerzero*(RR[0]+RR[1])+(ut[0]-ut[1])*pretimezero;
	rout = rzero_det + beta*(ddPe[0]+ddPe[1])/(ut[0]-ut[1]);
	rzero = (RR[0]+RR[1]) + 1.L/(1.L/(halfwidth/beta*halfwidth/beta*(ut[0]-ut[1])/(ddPe[0]+ddPe[1])) + 1.L/(rzero_det));
	prevdw=192.0L*mm[0]*mm[1]*gam*hamaker_constant*hamaker_constant;
	Peclet=(RR[0]+RR[1])*(ut[0]-ut[1])/(ddPe[0]+ddPe[1]);

	fprintf(stderr, "mathcalA = %.14Le\n", aaa);
	fprintf(stderr, "radius_ratio = %.14Le\n", gamra);
	fprintf(stderr, "R1= %.14Le and R2=%.14Le \n", RR[0],RR[1]);
	fprintf(stderr, "HalfW=%Le R0+R1=%Le\n",halfwidth,RR[0]+RR[1]);
	fprintf(stderr, "rzero = %Le\n", rzero);
	fprintf(stderr, "grav_const/Kn^3 = %Le\n", ggg);
	fprintf(stderr, "mathcalN = %Le\n", nnn);
	fprintf(stderr, "lambda = %Le\n", lambda);
	fprintf(stderr, "mathcalK = %Le\n", mathcalK);
	fprintf(stderr, "density_ratio = %Le\n", ddd);
	fprintf(stderr, "ddPe1 = %Le\n", ddPe[0]);
	fprintf(stderr, "ddPe2 = %Le\n", ddPe[1]);
	fprintf(stderr, "ut0 = %Le\n", ut[0]);
	fprintf(stderr, "ut1 = %Le\n", ut[1]);
	fprintf(stderr, "Re 0 = %Le\n", RR[0]*ut[0]/ddd);
	fprintf(stderr, "Re 1 = %Le\n", RR[1]*ut[1]/ddd);
	fprintf(stderr, "Temps 1/(u0-u1) = %Le\n", 1.0L/(ut[0]-ut[1]));
	fprintf(stderr, "Tau 0 = %Le Tau 1 = %Le\n", tau[0], tau[1]);
	fprintf(stderr, "LarDiff = %Le  W/2= %Le\n",sqrtl((ddPe[0]+ddPe[1])*(2.0L*rzero/(ut[0]-ut[1]))),halfwidth);
}

void derivative(long double *x, long double *deriv_x)
{
	/*
	 Computes the derivative of vector x.
	 x[0] -> x0
	 x[1] -> vx0
	 x[2] -> x1
	 x[3] -> vx1
	 x[4] -> y0
	 x[5] -> vy0
	 x[6] -> y1
	 x[7] -> vy1
	 x[8] -> z0
	 x[9] -> vz0
	 x[10] -> z1
	 x[11] -> vz1
	 x[12] -> Z redondant; coupled or not we will see
	 */
	long ii,jj;
	long double h;
	long double vecr[3];
	long double eth[3];
	long double er[3];
	long double vnorm[2];
	long double ww[2][3];
	long double vjr,vjth,wir,with,phi;
	long double unsvjth;
	long double rrca,rlo,unsr,rdot;
	long double gprime,denovdw,fvdw;
	long double upsilon,hsl;

	//Derivative of coordinates
	for (ii=0;ii<=5;ii++)
		deriv_x[2*ii]=x[2*ii+1];

	//relative position between particles
	rrca=0.0L;
	for (ii=0;ii<=2;ii++)
	{
		vecr[ii]=x[4*ii+2]-x[4*ii]; //vec r(1)-vec r(0)
		rrca+=vecr[ii]*vecr[ii];
	}
	rlo=sqrtl(rrca);// r
	unsr=1.0L/rlo;
	h=rlo-RR[0]-RR[1]; //Gap, to be replaced by dynamic variable Z in case of failure
	x[12]=h;
	if (h<epshh)
		h=epshh;

	for (ii=0;ii<2;ii++)
	{
		if (ii==0)
		{
			rdot=0.0L;
			for (jj=0;jj<=2;jj++)
			{
				er[jj]=vecr[jj]*unsr;// unit radial vector
				rdot+=(x[4*jj+3]-x[4*jj+1])*er[jj]; //normal derivative
			}
		}
		else
			for (jj=0;jj<=2;jj++)
				er[jj]=-vecr[jj]*unsr;// unit radial vector
		vnorm[ii]=sqrtl(x[1+2*ii]*x[1+2*ii]+x[5+2*ii]*x[5+2*ii]+(x[9+2*ii]+ugal)*(x[9+2*ii]+ugal));
		for (jj=0;jj<=2;jj++)
			eth[jj]=x[4*jj+1+2*ii];
		eth[2]+=ugal;
		vjr=0.0L;
		for (jj=0;jj<=2;jj++)
			vjr+=eth[jj]*er[jj];
		vjth=0.0L;
		for (jj=0;jj<=2;jj++)
		{
			eth[jj]-=vjr*er[jj];
			vjth+=eth[jj]*eth[jj];
		}
		vjth=sqrtl(vjth);
		unsvjth=1.0L/(epshh+vjth);
		for (jj=0;jj<=2;jj++)
			eth[jj]*=unsvjth;

		phi=expl(-0.5L*rlo*(vnorm[ii]+vjr)/ddd);
		wir=0.0625L/(rlo*rrca*ddd*ddd)*RR[ii]*(-24.0L*ddd*ddd*ddd*(-1.0L + phi)*rlo - 4.0L*ddd*ddd*(3*phi*rrca*(vnorm[ii] - vjr) + 2.0L*RR[ii]*RR[ii]*vjr + 2.0L*phi*RR[1-ii]*RR[1-ii]*vjr) -
											   phi*rrca*RR[1-ii]*RR[1-ii]*vnorm[ii]*vjth*vjth - 2.0L*ddd*phi*rlo*RR[1-ii]*RR[1-ii]*(2.0L*vnorm[ii]*vjr + vjth*vjth));
		with=0.0625L/(rlo*rrca*ddd*ddd)*RR[ii]*( (4.0L*ddd*ddd*(RR[ii]*RR[ii] + phi*(3.0L*rrca + RR[1-ii]*RR[1-ii])) + 2.0L*ddd*phi*rlo*RR[1-ii]*RR[1-ii]*(vnorm[ii] + vjr) + phi*rrca*RR[1-ii]*RR[1-ii]*vnorm[ii]*(vnorm[ii] + vjr))*vjth);
		for (jj=0;jj<=2;jj++)
			ww[1-ii][jj]=(wir*er[jj]+with*eth[jj]);
	}

	// Lubrication force
	upsilon = 1.0L+2.0L/M_PI*log1pl(1.0L/(3.0L*aaa*h));
	hsl=aaa*(h+(8.0L/7.0L)*sqrtl(h)/nnn)/6.0L;
	gprime = aaa/(3.0L*upsilon)*((1.0L+hsl)*log1pl(1.0L/hsl)-1.0L);

	//Van der Waals force
	denovdw=(2.0L*RR[0]+h)*(2.0L*RR[1]+h)*(2.0L*(RR[0]+RR[1])+h)*(hamaker_constant+h*aaa);
	fvdw = gprime*rdot+prevdw*rlo/(denovdw*denovdw);

	//Long range force
	deriv_x[1]=4.5L*(RR[0]*((1.0L+2.0L*cd[0]*vnorm[0])*ww[0][0]-(1.0L+cd[0]*vnorm[0])*x[1])-fvdw*er[0])/mm[0];
	deriv_x[3]=4.5L*(RR[1]*((1.0L+2.0L*cd[1]*vnorm[1])*ww[1][0]-(1.0L+cd[1]*vnorm[1])*x[3])+fvdw*er[0])/mm[1];
	deriv_x[5]=4.5L*(RR[0]*((1.0L+2.0L*cd[0]*vnorm[0])*ww[0][1]-(1.0L+cd[0]*vnorm[0])*x[5])-fvdw*er[1])/mm[0];
	deriv_x[7]=4.5L*(RR[1]*((1.0L+2.0L*cd[1]*vnorm[1])*ww[1][1]-(1.0L+cd[1]*vnorm[1])*x[7])+fvdw*er[1])/mm[1];
	deriv_x[9]=ggg+4.5L*(RR[0]*((1.0L+2.0L*cd[0]*vnorm[0])*ww[0][2]-(1.0L+cd[0]*vnorm[0])*(x[9]+ugal))-fvdw*er[2])/mm[0];
	deriv_x[11]=ggg+4.5L*(RR[1]*((1.0L+2.0L*cd[1]*vnorm[1])*ww[1][2]-(1.0L+cd[1]*vnorm[1])*(x[11]+ugal))+fvdw*er[2])/mm[1];
}

void resistance(long double *x)
{
	long ii,jj,kk,ll,tour;
	long double h;
	long double sij[2][2][2];
	long double cor[2][4][4];
	long double fabr[2][4][4];
	long double alea;
	long double vecr[4];
	long double gag[4];
	long double edir[4][4];
	long double vjr,vjth;
	long double unsvjth;
	long double rrca,rlo,unsr;
	long double gprime;
	long double upsilon,hsl;
	long double magni1;
	double long x1,y1;
	long double dd, dc;
	long double cosh_dd_dt_2, cosh_dd_dt, sinh_dd_dt_2, sinh_dd_dt, exp_s00_s11_2, exp_s00_s11;


	/*
	 Computes the derivative of vector x.
	 x[0] -> x0
	 x[1] -> vx0
	 x[2] -> x1
	 x[3] -> vx1
	 x[4] -> y0
	 x[5] -> vy0
	 x[6] -> y1
	 x[7] -> vy1
	 x[8] -> z0
	 x[9] -> vz0
	 x[10] -> z1
	 x[11] -> vz1
	 x[12] -> Z redondant; coupled or not we will see
	 */

	//relative position between particles
	rrca=0.0L;
	for (ii=0;ii<=2;ii++)
	{
		vecr[ii]=x[4*ii+2]-x[4*ii]; //vec r(1)-vec r(0)
		rrca+=vecr[ii]*vecr[ii];
	}
	rlo=sqrtl(rrca);// r
	unsr=1.0L/rlo;
	h=rlo-RR[0]-RR[1]; //Gap, to be replaced by dynamic variable Z in case of failure
	x[12]=h;
	if (h<epshh)
		h=epshh;

	// unit radial vector
	for (jj=0;jj<=2;jj++)
		edir[0][jj]=vecr[jj]*unsr;
	//unit transverse vector
	if (fabsl(edir[0][2])>0.9L)
	{
		edir[1][0]=1.0L;
		edir[1][1]=0.0L;
		edir[1][2]=0.0L;
	}
	else
	{
		edir[1][0]=0.0L;
		edir[1][1]=0.0L;
		edir[1][2]=1.0L;
	}
	vjr=0.0L;
	for (jj=0;jj<=2;jj++)
		vjr+=edir[1][jj]*edir[0][jj];
	vjth=0.0L;
	for (jj=0;jj<=2;jj++)
	{
		edir[1][jj]-=vjr*edir[0][jj];
		vjth+=edir[1][jj]*edir[1][jj];
	}
	vjth=sqrtl(vjth);
	unsvjth=1.0L/vjth;
	for (jj=0;jj<=2;jj++)
		edir[1][jj]*=unsvjth;
	//Last vector
	edir[2][0]=edir[1][1]*edir[0][2]-edir[1][2]*edir[0][1];
	edir[2][1]=edir[1][2]*edir[0][0]-edir[1][0]*edir[0][2];
	edir[2][2]=edir[1][0]*edir[0][1]-edir[1][1]*edir[0][0];

	// Lubrication force
	upsilon = 1.0L+2.0L/M_PI*log1pl(1.0L/(3.0L*aaa*h));
	hsl=aaa*(h+(8.0L/7.0L)*sqrtl(h)/nnn)/6.0L;
	gprime = aaa/(3.0L*upsilon)*((1.0L+hsl)*log1pl(1.0L/hsl)-1.0L);

	//Modified Diffusion
	// Radial direction 0
	for (kk=0;kk<2;kk++)
		sij[0][kk][kk]=RR[kk]+gprime;
	sij[0][0][1]=-gprime-0.5L*RR[0]*RR[1]/rlo*(3.0L-(RR[0]*RR[0]+RR[1]*RR[1])/rrca);
	sij[0][1][0]=sij[0][0][1];

	// Perpendicular direction 1
	for (kk=0;kk<2;kk++)
		sij[1][kk][kk]=RR[kk];

	sij[1][0][1]=-0.25L*RR[0]*RR[1]/rlo*(3.0L+(RR[0]*RR[0]+RR[1]*RR[1])/rrca);
	sij[1][1][0]=sij[1][0][1];
	for (kk=0;kk<2;kk++)
		for (ii=0;ii<2;ii++)
			for (jj=0;jj<2;jj++)
				sij[kk][ii][jj]*=unsm[ii];

	for (kk=0;kk<2;kk++)
	{
		dc =4*sij[kk][0][1]*sij[kk][1][0] + powl(sij[kk][0][0] - sij[kk][1][1],2);
		dd = sqrtl(dc);
		cosh_dd_dt_2 = coshl((dd*dt_2)/2.);
		cosh_dd_dt = coshl(dd*dt_2);
		sinh_dd_dt_2 = sinhl((dd*dt_2)/2.);
		sinh_dd_dt = sinhl(dd*dt_2);
		exp_s00_s11_2 = expl(((sij[kk][0][0] + sij[kk][1][1])*dt_2)/2.);
		exp_s00_s11 = expl((sij[kk][0][0] + sij[kk][1][1])*dt_2);

		//rr11
		cor[kk][0][0]=(-64*(-(sij[kk][0][1]*sij[kk][1][0]) + sij[kk][0][0]*sij[kk][1][1])*(2*sij[kk][0][1]*(ktsm[1]*sij[kk][0][1] - ktsm[0]*sij[kk][1][0])*(sij[kk][0][1]*sij[kk][1][0] - sij[kk][0][0]*sij[kk][1][1]) +dc*exp_s00_s11*(ktsm[1]*powl(sij[kk][0][1],2) + ktsm[0]*powl(sij[kk][1][1],2)*(3 - 2*sij[kk][0][0]*dt_2) + 2*ktsm[0]*sij[kk][0][1]*(sij[kk][1][0] + sij[kk][1][0]*sij[kk][1][1]*dt_2)) -2*dc*exp_s00_s11_2*(ktsm[1]*powl(sij[kk][0][1],2) + ktsm[0]*sij[kk][0][1]*sij[kk][1][0] + 2*ktsm[0]*powl(sij[kk][1][1],2))*cosh_dd_dt_2 +(ktsm[1]*powl(sij[kk][0][1],2)*(powl(sij[kk][0][0],2) + 2*sij[kk][0][1]*sij[kk][1][0] + powl(sij[kk][1][1],2)) + ktsm[0]*(2*powl(sij[kk][0][1],2)*powl(sij[kk][1][0],2) + powl(sij[kk][0][0] - sij[kk][1][1],2)*powl(sij[kk][1][1],2) + 2*sij[kk][0][1]*sij[kk][1][0]*sij[kk][1][1]*(-sij[kk][0][0] + 2*sij[kk][1][1])))*cosh_dd_dt + dd*(-2*exp_s00_s11_2*(ktsm[1]*powl(sij[kk][0][1],2)*(sij[kk][0][0] + sij[kk][1][1]) + ktsm[0]*(sij[kk][0][0]*sij[kk][0][1]*sij[kk][1][0] + 5*sij[kk][0][1]*sij[kk][1][0]*sij[kk][1][1] - 2*sij[kk][0][0]*powl(sij[kk][1][1],2) + 2*powl(sij[kk][1][1],3)))*sinh_dd_dt_2 + (ktsm[1]*powl(sij[kk][0][1],2)*(sij[kk][0][0] + sij[kk][1][1]) + ktsm[0]*sij[kk][1][1]*(2*sij[kk][0][1]*sij[kk][1][0] - sij[kk][0][0]*sij[kk][1][1] + powl(sij[kk][1][1],2)))*sinh_dd_dt)))/(dc*exp_s00_s11*powl(-4*sij[kk][0][1]*sij[kk][1][0] + 4*sij[kk][0][0]*sij[kk][1][1],3));

		//rv11
		cor[kk][1][0]=(dc*exp_s00_s11*ktsm[0]*sij[kk][1][1] - sij[kk][0][1]*(ktsm[1]*sij[kk][0][1] - ktsm[0]*sij[kk][1][0])*(sij[kk][0][0] + sij[kk][1][1]) - 2*dc*exp_s00_s11_2*ktsm[0]*sij[kk][1][1]*cosh_dd_dt_2 +(ktsm[1]*powl(sij[kk][0][1],2)*(sij[kk][0][0] + sij[kk][1][1]) + ktsm[0]*(powl(sij[kk][0][0],2)*sij[kk][1][1] + 3*sij[kk][0][1]*sij[kk][1][0]*sij[kk][1][1] + powl(sij[kk][1][1],3) - sij[kk][0][0]*(sij[kk][0][1]*sij[kk][1][0] + 2*powl(sij[kk][1][1],2))))*cosh_dd_dt +2*dd*(ktsm[1]*powl(sij[kk][0][1],2) + ktsm[0]*sij[kk][0][1]*sij[kk][1][0] + ktsm[0]*sij[kk][1][1]*(-sij[kk][0][0] + sij[kk][1][1]))*(-exp_s00_s11_2 + cosh_dd_dt_2)*sinh_dd_dt_2)/(dc*exp_s00_s11*(-(sij[kk][0][1]*sij[kk][1][0]) + sij[kk][0][0]*sij[kk][1][1]));

		//vv11
		cor[kk][1][1]=(dc*exp_s00_s11*ktsm[0] + 2*ktsm[1]*powl(sij[kk][0][1],2) - 2*ktsm[0]*sij[kk][0][1]*sij[kk][1][0] - (2*ktsm[1]*powl(sij[kk][0][1],2) + ktsm[0]*(2*sij[kk][0][1]*sij[kk][1][0] + powl(sij[kk][0][0] - sij[kk][1][1],2)))*cosh_dd_dt +dd*ktsm[0]*(sij[kk][0][0] - sij[kk][1][1])*sinh_dd_dt)/(dc*exp_s00_s11);

		//rr12
		cor[kk][2][0]=(64*(-(sij[kk][0][1]*sij[kk][1][0]) + sij[kk][0][0]*sij[kk][1][1])*((ktsm[1]*sij[kk][0][1] - ktsm[0]*sij[kk][1][0])*(sij[kk][0][0] - sij[kk][1][1])*(-(sij[kk][0][1]*sij[kk][1][0]) + sij[kk][0][0]*sij[kk][1][1]) -dc*exp_s00_s11*(ktsm[1]*sij[kk][0][1]*(2*sij[kk][0][0] + sij[kk][1][1] + sij[kk][0][1]*sij[kk][1][0]*dt_2 - sij[kk][0][0]*sij[kk][1][1]*dt_2) + ktsm[0]*sij[kk][1][0]*(sij[kk][0][0] + 2*sij[kk][1][1] + sij[kk][0][1]*sij[kk][1][0]*dt_2 - sij[kk][0][0]*sij[kk][1][1]*dt_2)) +dc*exp_s00_s11_2*(ktsm[1]*sij[kk][0][1]*(3*sij[kk][0][0] + sij[kk][1][1]) + ktsm[0]*sij[kk][1][0]*(sij[kk][0][0] + 3*sij[kk][1][1]))*cosh_dd_dt_2 -(ktsm[1]*sij[kk][0][1]*(powl(sij[kk][0][0],3) + 3*sij[kk][0][0]*sij[kk][0][1]*sij[kk][1][0] - powl(sij[kk][0][0],2)*sij[kk][1][1] + sij[kk][0][1]*sij[kk][1][0]*sij[kk][1][1]) + ktsm[0]*sij[kk][1][0]*(sij[kk][0][0]*sij[kk][0][1]*sij[kk][1][0] + 3*sij[kk][0][1]*sij[kk][1][0]*sij[kk][1][1] - sij[kk][0][0]*powl(sij[kk][1][1],2) + powl(sij[kk][1][1],3)))*cosh_dd_dt +dd*(exp_s00_s11_2*(ktsm[1]*sij[kk][0][1]*(3*powl(sij[kk][0][0],2) + 4*sij[kk][0][1]*sij[kk][1][0] + powl(sij[kk][1][1],2)) + ktsm[0]*sij[kk][1][0]*(powl(sij[kk][0][0],2) + 4*sij[kk][0][1]*sij[kk][1][0] + 3*powl(sij[kk][1][1],2)))*sinh_dd_dt_2 -(ktsm[1]*sij[kk][0][1]*(powl(sij[kk][0][0],2) + sij[kk][0][1]*sij[kk][1][0]) + ktsm[0]*sij[kk][1][0]*(sij[kk][0][1]*sij[kk][1][0] + powl(sij[kk][1][1],2)))*sinh_dd_dt)))/(dc*exp_s00_s11*powl(4*sij[kk][0][1]*sij[kk][1][0] - 4*sij[kk][0][0]*sij[kk][1][1],3));

		//rv21
		cor[kk][2][1]=(-(dc*exp_s00_s11*ktsm[1]*sij[kk][0][1]) +(ktsm[1]*sij[kk][0][1] - ktsm[0]*sij[kk][1][0])*(powl(sij[kk][0][0],2) + 2*sij[kk][0][1]*sij[kk][1][0] - sij[kk][0][0]*sij[kk][1][1]) +dc*exp_s00_s11_2*(ktsm[1]*sij[kk][0][1] + ktsm[0]*sij[kk][1][0])*cosh_dd_dt_2 -(ktsm[1]*sij[kk][0][1]*(powl(sij[kk][0][0],2) + 2*sij[kk][0][1]*sij[kk][1][0] - sij[kk][0][0]*sij[kk][1][1]) +ktsm[0]*sij[kk][1][0]*(2*sij[kk][0][1]*sij[kk][1][0] - sij[kk][0][0]*sij[kk][1][1] + powl(sij[kk][1][1],2)))*cosh_dd_dt +dd*(exp_s00_s11_2*(ktsm[0]*sij[kk][1][0]*(-sij[kk][0][0] + sij[kk][1][1]) + ktsm[1]*sij[kk][0][1]*(3*sij[kk][0][0] + sij[kk][1][1]))*sinh_dd_dt_2 -(ktsm[1]*sij[kk][0][0]*sij[kk][0][1] + ktsm[0]*sij[kk][1][0]*sij[kk][1][1])*sinh_dd_dt))/(dc*exp_s00_s11*(-(sij[kk][0][1]*sij[kk][1][0]) + sij[kk][0][0]*sij[kk][1][1]));

		//rr22
		cor[kk][2][2]=(-64*(-(sij[kk][0][1]*sij[kk][1][0]) + sij[kk][0][0]*sij[kk][1][1])*(2*sij[kk][1][0]*(-(ktsm[1]*sij[kk][0][1]) + ktsm[0]*sij[kk][1][0])*(sij[kk][0][1]*sij[kk][1][0] - sij[kk][0][0]*sij[kk][1][1]) +dc*exp_s00_s11*(2*ktsm[1]*sij[kk][0][1]*sij[kk][1][0] + ktsm[0]*powl(sij[kk][1][0],2) + ktsm[1]*sij[kk][0][0]*(2*sij[kk][0][1]*sij[kk][1][0]*dt_2 + sij[kk][0][0]*(3 - 2*sij[kk][1][1]*dt_2))) -2*dc*exp_s00_s11_2*(2*ktsm[1]*powl(sij[kk][0][0],2) + ktsm[1]*sij[kk][0][1]*sij[kk][1][0] + ktsm[0]*powl(sij[kk][1][0],2))*cosh_dd_dt_2 +(ktsm[0]*powl(sij[kk][1][0],2)*(powl(sij[kk][0][0],2) + 2*sij[kk][0][1]*sij[kk][1][0] + powl(sij[kk][1][1],2)) +ktsm[1]*(powl(sij[kk][0][0],4) + 4*powl(sij[kk][0][0],2)*sij[kk][0][1]*sij[kk][1][0] + 2*powl(sij[kk][0][1],2)*powl(sij[kk][1][0],2) - 2*sij[kk][0][0]*(powl(sij[kk][0][0],2) + sij[kk][0][1]*sij[kk][1][0])*sij[kk][1][1] + powl(sij[kk][0][0],2)*powl(sij[kk][1][1],2)))*cosh_dd_dt +dd*(-2*exp_s00_s11_2*(ktsm[0]*powl(sij[kk][1][0],2)*(sij[kk][0][0] + sij[kk][1][1]) + ktsm[1]*(2*powl(sij[kk][0][0],3) + 5*sij[kk][0][0]*sij[kk][0][1]*sij[kk][1][0] - 2*powl(sij[kk][0][0],2)*sij[kk][1][1] + sij[kk][0][1]*sij[kk][1][0]*sij[kk][1][1]))*sinh_dd_dt_2 +(ktsm[0]*powl(sij[kk][1][0],2)*(sij[kk][0][0] + sij[kk][1][1]) + ktsm[1]*sij[kk][0][0]*(powl(sij[kk][0][0],2) + 2*sij[kk][0][1]*sij[kk][1][0] - sij[kk][0][0]*sij[kk][1][1]))*sinh_dd_dt)))/(dc*exp_s00_s11*powl(-4*sij[kk][0][1]*sij[kk][1][0] + 4*sij[kk][0][0]*sij[kk][1][1],3));

		//rv12
		cor[kk][3][0]=(-(dc*exp_s00_s11*ktsm[0]*sij[kk][1][0]) - (ktsm[1]*sij[kk][0][1] - ktsm[0]*sij[kk][1][0])*(2*sij[kk][0][1]*sij[kk][1][0] + sij[kk][1][1]*(-sij[kk][0][0] + sij[kk][1][1])) + dc*exp_s00_s11_2*(ktsm[1]*sij[kk][0][1] + ktsm[0]*sij[kk][1][0])*cosh_dd_dt_2 -(ktsm[1]*sij[kk][0][1]*(powl(sij[kk][0][0],2) + 2*sij[kk][0][1]*sij[kk][1][0] - sij[kk][0][0]*sij[kk][1][1]) + ktsm[0]*sij[kk][1][0]*(2*sij[kk][0][1]*sij[kk][1][0] - sij[kk][0][0]*sij[kk][1][1] + powl(sij[kk][1][1],2)))*cosh_dd_dt +dd*(exp_s00_s11_2*(ktsm[1]*sij[kk][0][1]*(sij[kk][0][0] - sij[kk][1][1]) + ktsm[0]*sij[kk][1][0]*(sij[kk][0][0] + 3*sij[kk][1][1]))*sinh_dd_dt_2 - (ktsm[1]*sij[kk][0][0]*sij[kk][0][1] + ktsm[0]*sij[kk][1][0]*sij[kk][1][1])*sinh_dd_dt))/(dc*exp_s00_s11*(-(sij[kk][0][1]*sij[kk][1][0]) + sij[kk][0][0]*sij[kk][1][1])) ;

		//vv12
		cor[kk][3][1]=((ktsm[1]*sij[kk][0][1] - ktsm[0]*sij[kk][1][0])*(sij[kk][0][0] - sij[kk][1][1])*(-1 + cosh_dd_dt) + dd*(ktsm[1]*sij[kk][0][1] + ktsm[0]*sij[kk][1][0])*sinh_dd_dt)/(dc*exp_s00_s11);

		//rv22
		cor[kk][3][2]=(dc*exp_s00_s11*ktsm[1]*sij[kk][0][0] + sij[kk][1][0]*(ktsm[1]*sij[kk][0][1] - ktsm[0]*sij[kk][1][0])*(sij[kk][0][0] + sij[kk][1][1]) - 2*dc*exp_s00_s11_2*ktsm[1]*sij[kk][0][0]*cosh_dd_dt_2 +(ktsm[0]*powl(sij[kk][1][0],2)*(sij[kk][0][0] + sij[kk][1][1]) + ktsm[1]*(powl(sij[kk][0][0],3) - 2*powl(sij[kk][0][0],2)*sij[kk][1][1] - sij[kk][0][1]*sij[kk][1][0]*sij[kk][1][1] + sij[kk][0][0]*(3*sij[kk][0][1]*sij[kk][1][0] + powl(sij[kk][1][1],2))))*cosh_dd_dt +2*dd*(ktsm[0]*powl(sij[kk][1][0],2) + ktsm[1]*(powl(sij[kk][0][0],2) + sij[kk][0][1]*sij[kk][1][0] - sij[kk][0][0]*sij[kk][1][1]))*(-exp_s00_s11_2 + cosh_dd_dt_2)*sinh_dd_dt_2)/(dc*exp_s00_s11*(-(sij[kk][0][1]*sij[kk][1][0]) + sij[kk][0][0]*sij[kk][1][1]));

		//vv22
		cor[kk][3][3]=(dc*exp_s00_s11*ktsm[1] - 2*ktsm[1]*sij[kk][0][1]*sij[kk][1][0] + 2*ktsm[0]*powl(sij[kk][1][0],2) - (2*ktsm[0]*powl(sij[kk][1][0],2) + ktsm[1]*(2*sij[kk][0][1]*sij[kk][1][0] + powl(sij[kk][0][0] - sij[kk][1][1],2)))*cosh_dd_dt +dd*ktsm[1]*(-sij[kk][0][0] + sij[kk][1][1])*sinh_dd_dt)/(dc*exp_s00_s11) ;

		for (ii=0;ii<4;ii++)
			for (jj=0;jj<=ii;jj++)
			{
				fabr[kk][ii][jj]=cor[kk][ii][jj];
				for (ll=0;ll<jj;ll++)
					fabr[kk][ii][jj]-=fabr[kk][ii][ll]*fabr[kk][jj][ll];
				if (jj<ii)
					fabr[kk][ii][jj]/=fabr[kk][jj][jj];
				else fabr[kk][ii][jj]=sqrtl(fabr[kk][ii][jj]);
			}
	}

	for (tour=0;tour<2;tour++)//Two half RK4 steps
	{
		for (ii=0;ii<=11;ii++)
			xvnoise[tour][ii]=0.0L;

		for (kk=0;kk<3;kk++)// Scan eigenaxes
		{
			//Axis type
			if (kk==2)
				ll=1;//Perpendicular = 1
			else
				ll=kk;
			// 4 random numbers
			for (ii=0;ii<2;ii++)
			{
				x1 = ran2(&islu);
				y1 = ran2(&islu);
				magni1 = sqrtl(-2.0L*logl(x1));
				gag[2*ii]=magni1*sinl(2.0L*M_PI*y1);
				gag[2*ii+1]=magni1*cosl(2.0L*M_PI*y1);
			}
			//Scan variable index, to be picked at random: x1,Vx1,x2,Vx2
			for (ii=0;ii<4;ii++)
			{
				alea=0.0L;
				for (jj=0;jj<=ii;jj++)
					alea+=fabr[ll][ii][jj]*gag[jj];
				//Scan x,y,z
				for (jj=0;jj<3;jj++)
					xvnoise[tour][4*jj+ii]+=alea*edir[kk][jj];
			}
		}
	}
}

void runge_kutta(long display_flag)
{
	long i,k,n;
	long double dxs[13],dxm[13],dxt[13],x[13];


	for (k=0;k<2;k++)
		for (i=0;i<=11;i++)
			xvnoise[k][i]=0.0L;

	simulation_time=0.0L;
	for (i=0;i<=11;i++)
		solution[i]=0.0L;
	solution[2]=deltax;
	solution[6]=deltay;
	solution[10]=rzero;
	solution[9]=ut[0]-ugal;
	solution[11]=ut[1]-ugal;

	if (display_flag>0)
	{
		fprintf(out,"WAVES/O/R/S tt\txx0\tvx0\txx1\tvx1\tzz0\tvz0\tzz1\tvz1\thh\taa0\taa1\n");
		fprintf(out,"BEGIN\n");
		fprintf(out,"%.14Le\t%.14Le\t%.14Le\t%.14Le\t%.14Le\t%.14Le\t%.14Le\t%.14Le\t%.14Le\t%.14Le\t%.14Le\t%.14Le\n",simulation_time,solution[0],solution[1],solution[2],solution[3],solution[8],solution[9],solution[10],solution[11],solution[12],0.0L,0.0L);
	}
	dt=epsdt;
	dt_2=0.5L*dt;
	seuilbas=1.0E1L;
	seuilhaut=1.0E42L;

	n=0;
	ChocBool=0;
	do
	{
		n++;
		for (i=0;i<=11;i++) x[i]=solution[i];
		derivative(x, dxs); /* k1=dt*dxs */
		solution[12]=x[12];

		/* dt=epsdt*MIN(1.0L,solution[12]+1.0L/aaa)/(ut[0]-ut[1]);*/
		if (solution[12]<seuilbas)
		{
			dt*=0.4L;
			dt_2=0.5L*dt;
			seuilhaut=seuilbas;
			if (seuilbas>1.0E-5L)
				seuilbas/=10.0L;
			else seuilbas=-1.0E40L;
			if (display_flag>0)
				printf("t=%Leh=%Le dt=%Le \n",simulation_time,solution[12],dt);
		}
		else if (solution[12]>seuilhaut)
		{
			dt*=2.5L;
			dt_2=0.5L*dt;
			seuilbas=seuilhaut;
			seuilhaut*=10.0L;
			if (display_flag>0)
				printf("t=%Leh=%Le dt=%Le \n",simulation_time,solution[12],dt);
		}

		if (solution[12]<=0.0L)
			ChocBool=1;
		else
		{
			resistance(x);//Compute xvnoise

			for (i=0;i<=11;i++) x[i]=solution[i]+dt_2*dxs[i]+xvnoise[0][i];
			derivative(x, dxt); /* k2=dt*dxt */
			for (i=0;i<=11;i++) x[i]=solution[i]+dt_2*dxt[i]+xvnoise[0][i];
			derivative(x, dxm); /* k3=dt*dxm */
			for (i=0;i<=11;i++) {x[i]=solution[i]+dt*dxm[i]+xvnoise[0][i]+xvnoise[1][i]; dxm[i] += dxt[i]; } /* k2+k3=dt*dxm */
			derivative(x, dxt);
			for (i=0;i<=11;i++) solution[i]+=dt*(dxs[i]+dxt[i]+2.0*dxm[i])/6.0+xvnoise[0][i]+xvnoise[1][i];
			simulation_time+=dt;
		}
		for (i=0; i<4; i++)
		{
			if (solution[2*i]>halfwidth)
				solution[2*i]-=width;
			if (solution[2*i]<-halfwidth)
				solution[2*i]+=width;
		}
		if ((display_flag>0)&&((n%1==0)||(solution[12]<0.1L)))
		{
			fprintf(out,"%.14Le\t%.14Le\t%.14Le\t%.14Le\t%.14Le\t%.14Le\t%.14Le\t%.14Le\t%.14Le\t%.14Le\t%.14Le\t%.14Le\n",simulation_time,solution[0],solution[1],solution[2],solution[3],solution[8],solution[9],solution[10],solution[11],solution[12],dxm[1]*dxm[1]+dxm[9]*dxm[9],dxm[3]*dxm[3]+dxm[11]*dxm[11]);
		}
	}
	while ((ChocBool==0)&&(solution[10]-solution[8]>-rout));
	fprintf(stderr, "%Le %Le %ld \n",solution[10]-solution[8],solution[12],ChocBool);
	if (display_flag>0)
	{
		fprintf(out,"END\n\n");
	}
}

void scan_trajectories(long flag)
{
	long i,j;
	long nb_touch;
	double long normaprobastop;

	nb_touch=0;
	normaprobastop=0.0L;

	init_geometry();

	if (flag>0)
	{
		fprintf(out,"deltax\tdeltay\tgamra\taaa\tsimulation_time\tdeltaxfin\tdeltayfin\trzero\tlambda\tChocBool\n");
	}

	for (i=-nb_trajectories;i<nb_trajectories;i++)
		for (j=-nb_trajectories;j<nb_trajectories;j++)
		{
			deltax=halfwidth*((double long)i+0.5L)/(double long)nb_trajectories;
			deltay=halfwidth*((double long)j+0.5L)/(double long)nb_trajectories;
			if ((deltax*deltax+deltay*deltay)<=(RR[0]+RR[1])*(RR[0]+RR[1]))
				normaprobastop+=1.0L;
			runge_kutta(0);
			nb_touch+=ChocBool;
			fprintf(stderr, "i=%ld j=%ld deltax=%Le deltay=%Le rzero = %Le tf = %Le x0-x1 = %.14Le y0-y1 = %.14Le z0-z1 = %.14Le choc = %ld\n", i,j, deltax,deltay,rzero, simulation_time, solution[0]-solution[2], solution[4]-solution[6], solution[8]-solution[10], ChocBool);
			if (flag>0)
			{
				fprintf(out, "%.14Le\t%.14Le\t%.14Le\t%.14Le\t%.14Le\t%.14Le\t%.14Le\t%.14Le\t%.14Le\t%ld\n",deltax, deltay, gamra, aaa,simulation_time,solution[0]-solution[2],solution[4]-solution[6],rzero, lambda, ChocBool);
				fflush(out);
			}
		}
	fprintf(stderr, "pro1=%Le",normaprobastop);
    // gamra aaa normaprobastop1 normaprobastop2 efficiency nb_touch ntot
    fprintf(out_log, "%.14Le\t%.14Le\t%.14Le\t", gamra, aaa, normaprobastop);
	normaprobastop=M_PI*((RR[0]+RR[1])/halfwidth)*(double long)nb_trajectories*((RR[0]+RR[1])/halfwidth)*(double long)nb_trajectories;
	fprintf(stderr, " pro2=%Le\n",normaprobastop);
	efficiency=(double long)nb_touch/normaprobastop;
	if (flag>0)
	{
		fflush(out);
	}
    fprintf(out_log, "%.14Le\t%.14Le\t%.14Le\t%.14Le\n", normaprobastop, efficiency, (long double)nb_touch, (long double)nb_trajectories);

}


void root_finder(long xouy)
{

	if (delta>(0.99L*(RR[0]+RR[1])/1.1L))
		delta=0.99L*(RR[0]+RR[1])/1.1L;
	if (xouy==0)
	{
		deltax=delta;
		deltay=0.0L;
	}
	else
	{

		deltax=0.0L;
		deltay=delta;
	}
	runge_kutta(0);
	if (ChocBool==0)
	{
		do
		{
			vd=delta;
			delta*=0.9L;
			if (xouy==0)
			{
				deltax=delta;
				deltay=0.0L;
			}
			else
			{

				deltax=0.0L;
				deltay=delta;
			}
			runge_kutta(0);
			printf("delta=%Le vd=%Le %Le %Le\n",delta,vd,solution[0],solution[2]);
		}
		while ((ChocBool==0)&&(delta>limdelta));
		vg=delta;
	}
	else
	{
		do
		{
			vg=delta;
			delta*=1.1L;
			if (xouy==0)
			{
				deltax=delta;
				deltay=0.0L;
			}
			else
			{

				deltax=0.0L;
				deltay=delta;
			}
			runge_kutta(0);
			printf("vg=%Le delta=%Le %Le %Le\n",vg,delta,solution[0],solution[2]);
		}
		while (ChocBool==1);
		vd=delta;
	}
	if (delta>limdelta)
	{
		do
		{
			delta=0.5L*(vg+vd);
			if (xouy==0)
			{
				deltax=delta;
				deltay=0.0L;
			}
			else
			{

				deltax=0.0L;
				deltay=delta;
			}
			runge_kutta(0);
			printf("vg=%Le delta=%Le vd=%Le %Le %Le\n",vg,delta,vd,solution[0],solution[2]);
			if (ChocBool==0) vd=delta;
			else vg=delta;
		}
		while ((vd-vg)>epsdelta*(vd+vg));
		delta=0.5L*(vg+vd);
	}
	else delta=limdelta;
	efficiency=(delta/(RR[0]+RR[1]))*(delta/(RR[0]+RR[1]));
	printf("Delta=%Le Efficiency=%Le",delta,efficiency);
}

/****************************************************************/
/**************************** calcul ****************************/
/****************************************************************/

int main(int argc, char *argv[])
{
	int opt;
	char *path = NULL;
	char *path_log = NULL;

	srand(time(NULL));
	islu=rand();
	fprintf(stderr, "beginning of the calculation\n");
	
	if(argc == 1)
	{
		fprintf(stderr, "Options :\n-g\t radius ratio > 1\n-a\t inverse Knudsen number (a/mean free path)\n-o\t output filename to save results for each try\n-l\toutput filename to save summary of results\n-n\tnumber of trajectories (side of the initial conditions square).\n");
		exit(0);
	}
	// Constants initialisation
	while ((opt = getopt(argc, argv, "a:g:o:n:l:")) != -1)
		switch (opt)
		{
			case 'a':
				aaa = strtold(optarg, NULL);
				break;
			case 'g':
				gamra = strtold(optarg, NULL);
				break;
			case 'o':
				path = optarg;
				fprintf(stderr, "path = %s\n", path);
				break;
			case 'l':
				path_log = optarg;
				fprintf(stderr, "path log = %s\n", path_log);
				break;
			case 'n':
				nb_trajectories = strtol(optarg, NULL, 10);
				break;
			case '?':
				if (optopt == 'a' || optopt == 'g' || optopt == 'o')
				{
					fprintf(stderr, "No input value.\n");
					fprintf(stderr, "Options :\n-g\t radius ratio > 1\n-a\t inverse Knudsen number (a/mean free path)\n-o\t output filename to save results for each try\n-l\toutput filename to save summary of results\n-n\tnumber of trajectories (side of the initial conditions square).\n");
				}
				else if (isprint (optopt))
					fprintf(stderr, "Unknown option `-%c'.\n", optopt);
				else
					fprintf(stderr, "Unknown option character `\\x%x'.\n", optopt);
				abort();
			default:
				abort();
		}

	if ((out = fopen (path, "w")) == NULL)
	{
		fprintf(stderr,"Probleme d'ouverture de %s\n", path);
		exit(1);
	}
	if ((out_log = fopen (path_log, "w")) == NULL)
	{
		fprintf(stderr,"Probleme d'ouverture de %s\n", path_log);
		exit(1);
	}

	#ifdef DIFFUSION
		fprintf(stderr, "Diffusion enabled.\n");
	#else
		fprintf(stderr, "Diffusion NOT enabled.\n");
	#endif
	scan_trajectories(1);

	fflush(out);
	fclose(out);
}


/*********** random number generators ***********/
long double ran0(void)
{
	const long iA=843314861;
	const long iB=453816693;
	const long iM=1073741824;
	long double aux,x;

	aux = 0.5L/(long double)(iM);
	isem = isem*iA + iB;
	if (isem < 0)
	{
		isem = (isem + iM )+iM;
	}
	x = isem*aux;
	return x;
}

/*********** random number generator between 0 and 1 ***********/
long double ran2(long *idum)
{
	const long IM1=2147483563, IM2=2147483399;
	const long IA1=40014, IA2=40692, IQ1=53668, IQ2=52774;
	const long IR1=12211, IR2=3791, NTAB=32, IMM1=IM1-1;
	const long NDIV=1+IMM1/NTAB;
	const long double EPS=3.0e-16L;
	const long double RNMX=1.0L-EPS;
	const long double AM=1.0L/(long double) IM1;
	static long idum2=123456789, iy=0;
	static long iv[32];
	long j,k;
	long double temp;

	if (*idum <= 0) {
		*idum = (*idum==0 ? 1 : -*idum);
		idum2 = *idum;
		for (j=NTAB+7; j>=0;j--) {
			k=*idum/IQ1;
			*idum=IA1*(*idum-k*IQ1)-k*IR1;
			if (*idum < 0) *idum+=IM1;
			if (j < NTAB) iv[j] = *idum;
		};
		iy=iv[0];
	};
	k=*idum/IQ1;
	*idum=IA1*(*idum-k*IQ1)-k*IR1;
	if(*idum < 0) *idum+=IM1;
	k=idum2/IQ2;
	idum2=IA2*(idum2-k*IQ2)-k*IR2;
	if (idum2 < 0) idum2 += IM2;
	j=iy/NDIV;
	iy=iv[j]-idum2;
	iv[j]=*idum;
	if(iy < 1) iy += IMM1;
	if((temp=AM*iy) > RNMX) return RNMX;
	else return temp;
}

double gaussian(long *idum)
{
	static long double t = 0.0L;
	long double x, w1, w2, r;

	if( t == 0 )
	{
		do
		{
			w1 = 2.0L * ran2(idum) - 1.0L;
			w2 = 2.0L * ran2(idum) - 1.0L;
			r = w1 * w1 + w2 * w2;
		}
		while( r >= 1.0 );
		r = sqrtl( -2.0*logl(r) / r );
		t = w2 * r;
		return(w1 * r);
	}
	else
	{
		x = t;
		t = 0.0;
		return(x);
	}
}
